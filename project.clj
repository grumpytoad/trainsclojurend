(defproject tw01 "0.1.0-SNAPSHOT"
  :description "Thoughtworks Train application assignment"
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main tw01.core)
