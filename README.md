To compile and run, please install [leiningen]( http://leiningen.org/):

```
lein run [file]
```

Where `file` contains a newline delimited list of graph edges. 

The tests are runnable through `lein test`.
