; copyright 2016 Niel Drummond

(ns tw01.graph-test
  (:require [clojure.test :refer :all]
            [tw01.graph :refer :all]))

(deftest hop-test
  (testing "A single hop should yield the expected result"
    (let [input [[{:path [:A :B] :dist 17}
                  {:path [:A :C] :dist 8}]]
          expected [[{:path [:A :B :C] :dist 21}
                     {:path [:A :B :D] :dist 18}
                     {:path [:A :C :B] :dist 16}]]
          vertices {:A {:B 17 :C 8}
                    :B {:C 4 :D 1}
                    :C {:B 8}}]

      (is (= expected (hop vertices input))))))

(deftest append-test
  (testing "A vertex should be added to the graph"
    (let [vertices {:A {:B 17}
                    :B {:C 4 :D 1}}
          input {:A {:C 8}}
          expected {:A {:B 17 :C 8}
                    :B {:C 4 :D 1}}]
      (is (= expected (append vertices input))))))

(deftest symbolise-test
  (testing "A shorthand string should convert into an edge"
    (is (= {:A {:B 5}} (symbolise "AB5")))))

(deftest exercise1
  (let [sample ["AB5" "BC4" "CD8" "DC8" "DE6" "AD5" "CE2" "EB3" "AE7"]
        vertices (reduce append {} (map symbolise sample))]

    (testing "The distance of route A-B-C should be 9"
      (is (= 9 (distance-of-route vertices [:A :B :C]))))

    (testing "The distance of route A-D should be 5"
      (is (= 5 (distance-of-route vertices [:A :D]))))

    (testing "The distance of route A-D-C should be 13"
      (is (= 13 (distance-of-route vertices [:A :D :C]))))

    (testing "The distance of route A-E-B-C-D should be 22"
      (is (= 22 (distance-of-route vertices [:A :E :B :C :D]))))

    (testing "Route A-E-D should throw an exception"
      (is (thrown? IllegalArgumentException (distance-of-route vertices [:A :E :D]))))

    (testing "The number of trips starting at C and ending at C with a maximum
             of 3 stops should be 2"
      (is (= 2 (->> 4 inc path-size-lt? in-step?
                    (routes-from-to vertices :C :C)
                    count))))

    (testing "The number of trips starting at A and ending at C with exactly 4
             stops should be 3"
      (is (= 3 (->> 4 inc path-size-lt? in-step?
                    (last-route-with-pred vertices :A)
                    (filter (path-ends-with? :C))
                    count))))

    (testing "The length of the shortest route (in terms of distance to travel)
             from A to C should be 9"
      (is (= 9 (shortest-route vertices :A :C))))

    (testing "The length of the shortest route (in terms of distance to travel)
             from B to B should be 9"
      (is (= 9 (shortest-route vertices :B :B))))

    (testing "The number of different routes from C to C with a distance of
             less than 30 should be 7"
      (is (= 7 (->> (dec 30) path-distance-gt? not-every-step?
                    (routes-from-to vertices :C :C)
                    (filter (path-distance-lt? 30))
                    count))))))
