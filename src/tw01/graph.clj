; copyright 2016 Niel Drummond

(ns tw01.graph)

(defn hop
  "Move one vertex to next neighbour, for all neighbours present in `vertices`.
  Return a flattened sequence of moves, each vertex identified by the keys
  `path` and `dist`, representing the path followed so far, and the total
  travel distance respectively."
  [vertices input]
  [(flatten
    (map (fn [{path :path total :dist}]
           (let [neighbours (get vertices (last path))]
             (map (fn [[neighbour distance]]
                    {:path (conj path neighbour) :dist (+ total distance)}) neighbours)))
         (last input)))])

(defn journey-seq
  "Return a lazy sequence of moves, based on a graph of `vertices` and a start
  keyword `input`."
  [vertices input] (concat input (lazy-seq (journey-seq vertices (hop vertices input)))))

(defn append
  "Reducer for building a graph from a set of edges."
  [vertices input]
  (into
   vertices
   (map (fn [[origin neighbour]]
          [origin (into (get vertices origin {}) neighbour)]) input)))

(defn symbolise
  "Convert a string to an edge, for example 'AB17' would become `{:A {:B 17}}`."
  [token]
  (let [[origin destination distance] (into [] token)]
    {(keyword (str origin)) {(keyword (str destination)) (Integer. (str distance))}}))

(defn starts-from [vertex] [[{:path [vertex] :dist 0}]])

(defn on-path? [route] (fn [vertex] (-> vertex :path (= route))))

(defn path-ends-with? [end] (fn [vertex] (some->> vertex :path (drop 1) last (= end))))

(defn not-in-step? [cb] (fn [step] (-> cb (some step) not)))

(defn in-step? [cb] (fn [step] (-> cb (some step))))

(defn not-every-step? [cb] (fn [step] (-> cb (not-every? step))))

(defn path-size-op? [op size] (fn [vertex] (-> vertex :path count (op size))))
(def path-size-lt? (partial path-size-op? <))
(def path-size-gt? (partial path-size-op? >))

(defn path-distance-op? [op dist] (fn [vertex] (-> vertex :dist (op dist))))
(def path-distance-lt? (partial path-distance-op? <))
(def path-distance-gt? (partial path-distance-op? >))

(defn routes-with-pred
  "Returns all iteration of moves as a nested sequence, through a graph of
  `vertices`, commencing from a `start` keyword, bounded by any number of
  predicates."
  [vertices start & pred]
  (take-while
   (comp (partial every? true?)
         (apply juxt pred))
   (journey-seq vertices (starts-from start))))

(defn last-route-with-pred
  "Returns the last iteration of moves as a sequence, through a graph of
  `vertices`, commencing from a `start` keyword, bounded by any number of
  predicates."
  [vertices start & pred]
  (first
   (drop-while
    (comp (partial every? true?)
          (apply juxt pred))
    (journey-seq vertices (starts-from start)))))

(defn distance-of-route
  "Returns the distance for a `route`, or list of moves (for example `[:A :B
  :C]`), through a graph of `vertices`. If the route is not resolvable, an
  IllegalArgumentException is thrown."
  [vertices route]
  (or
   (some->
    (filter (on-path? route)
            (last-route-with-pred vertices (first route)
                                  (in-step? (path-size-lt? (count route)))
                                  (not-in-step? (on-path? route))))
    not-empty
    first
    :dist)
   (throw (IllegalArgumentException. "NO SUCH ROUTE"))))

(defn routes-from-to
  "Returns a flat list of moves, through a graph of `vertices`, commencing from
  a `start` keyword and landing at an `end` keyword, bounded by any number of
  predicates."
  [vertices start end & pred]
  (flatten (map (partial filter (path-ends-with? end))
                (apply routes-with-pred vertices start pred))))

(defn shortest-route
  "Determines the shortest move determined by total distance, through a graph
  of `vertices`, commencing from a `start` keyword and landing at an `end`
  keyword."
  [vertices start end]
  (some->> (last-route-with-pred vertices start (not-in-step? (path-ends-with? end)))
           (sort-by :dist)
           first
           :dist))
