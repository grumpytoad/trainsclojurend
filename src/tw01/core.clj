; copyright 2016 Niel Drummond

(ns tw01.core
  (:require [tw01.graph :refer :all]))

(defn output
  [msg cb]
  (try (println (str msg (cb)))
       (catch IllegalArgumentException e
         (println (str msg (.getMessage e))))))

(defn usage []
  (println "Usage: lein run [file]
where 'file' contains newline separated strings as edges.
For example: 
AB4
CD3
AC17"))

(defn -main
  [& args]

  (let [sample (or (some-> args not-empty first clojure.java.io/file slurp clojure.string/split-lines)
                   (do (usage) (System/exit 1)))
        vertices (reduce append {} (map symbolise sample))]

    (output "The distance of route A-B-C is " #(distance-of-route vertices [:A :B :C]))
    (output "The distance of route A-D is " #(distance-of-route vertices [:A :D]))
    (output "The distance of route A-D-C is " #(distance-of-route vertices [:A :D :C]))
    (output "The distance of route A-E-B-C-D is " #(distance-of-route vertices [:A :E :B :C :D]))
    (output "The distance of route A-E-D is " #(distance-of-route vertices [:A :E :D]))
    (output "The number of trips starting at C and ending at C with a maximum of 3 stops is "
            #(->> 4 inc path-size-lt? in-step?
                  (routes-from-to vertices :C :C)
                  count))

    (output "The number of trips starting at A and ending at C with exactly 4 stops is "
            #(->> 4 inc path-size-lt? in-step?
                  (last-route-with-pred vertices :A)
                  (filter (path-ends-with? :C))
                  count))

    (output "The length of the shortest route (in terms of distance to travel) from A to C is "
            #(shortest-route vertices :A :C))

    (output "The length of the shortest route (in terms of distance to travel) from B to B is "
            #(shortest-route vertices :B :B))

    (output "The number of different routes from C to C with a distance of less than 30 is "
            #(->> 30 path-distance-lt? in-step?
                  (routes-from-to vertices :C :C)
                  (filter (path-distance-lt? 30))
                  count))))
