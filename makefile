all: zip

clean:
	rm -f TrainsClojureND.zip

zip: clean
	zip -r TrainsClojureND src test project.clj README.md sample
